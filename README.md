# RWTH Downloader
A bookmarklet to extract the source mp4 file from an embedded Opencast video on
RWTH's Moodle platform.

## About
RWTH currently uses the Opencast system to distribute videos on its Moodle
platform. However, Opencast's default video player, Paella, is lacking in some
regards, mainly that it covers the screen when pausing and that it does not
allow for the user to download the video. This bookmarklet allows you to instead
use your browser's default viewer, which is most likely better and allows you to
save the video to disk.

## Usage
Usage instructions can be found [here](https://qnemann.pages.rwth-aachen.de/rwthdl/).
