if (window.location.hostname !== 'engage.streaming.rwth-aachen.de') {
  // Find an Paella Player iframe and open it in its own tab/window.
  const iframe = document.querySelector('iframe.ocplayer')
  if (iframe) {
    window.open(iframe.src)
  } else {
    alert('Did not find any embedded Opencast videos.')
  }
} else {
  // Fetch the URL of and redirect to the source mp4.

  function selectTrack(tracks) {
    // Extract the resolution selected in the Paella Player.
    const span = document.querySelector('#buttonPlugin12 > span')
    const prefRes = span && (span.innerText.match(/^(\d+)p$/) || [])[1]

    if (prefRes) {
      // If possible, select a track matching the player's resolution.
      const t = tracks.find(t => t.video.resolution.split('x').pop() === prefRes)
      if (t) { return t }
      console.log('Could not find an mp4 matching the selected resolution.')
    }

    // Select the track with the highest resolution by default.
    const res = t => Number(t.video.resolution.split('x').pop())
    return tracks.reduce((a, b) => res(a) > res(b) ? a : b)
  }

  const params = new URLSearchParams(window.location.search)
  const id = params.get('id')
  fetch(`https://engage.streaming.rwth-aachen.de/search/episode.json?id=${id}`)
    .then(res => res.json())
    .then(json => {
      const tracks = json['search-results']['result']['mediapackage']['media']['track']
        .filter(t => t.url.endsWith('.mp4'))
      const track = selectTrack(tracks)
      console.log('Selected resolution:', track.video.resolution)
      window.location.href = track.url
    })
}
