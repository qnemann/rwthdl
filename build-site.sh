#!/bin/sh
sed "s/{{ bookmarklet }}/$(npx bookmarklet rwthdl.js)/g" usage-template.md \
  | cat README-base.md - \
  | pandoc -s -c style.css -o public/index.html --metadata pagetitle='RWTH Downloader'
