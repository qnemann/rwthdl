## Usage
1. Drag this link: <a style="font-weight: bold" href="{{ bookmarklet }}">RWTH Video</a> into your bookmarks toolbar.
2. On an RWTH site with an embedded Opencast video, click the bookmark. This will open the same player in a new tab/window.
3. Click the bookmark again to view the source mp4 file directly.

The mp4 will have the highest available resolution by default. To select a lower resolution, change the resolution in the Paella Player after step 2.
